﻿SET NAMES 'utf8';
USE ciclistas;

-- 1	Listar las edades de los ciclistas (sin repetidos).
  
  SELECT DISTINCT
      c.edad  -- campos (proyeccion)
    FROM 
      ciclista c; -- tabla (relacion)

-- 2	Listar las edades de los ciclistas de Artiach
  
SELECT DISTINCT
    c.edad                  -- proyeccion
  FROM 
    ciclista c              -- tabla
  WHERE
    c.nomequipo='Artiach';  -- seleccion (expresion)


  -- 3 
  -- Listar las edades de los ciclistas 
  -- de Artiach o de Amore Vita

  -- Realizado con OR

    SELECT DISTINCT
        c.edad  
      FROM 
        ciclista c
      WHERE                     -- hay que colocar una expresion (true, false)
        c.nomequipo='Artiach'
          OR 
        c.nomequipo='Amore Vita';


  -- Realizado con IN

    SELECT DISTINCT  
        c.edad
      FROM 
        ciclista c
      WHERE 
        c.nomequipo IN ('Artiach','Amore Vita');
    

  -- Realizado con UNION

  SELECT DISTINCT
      c.edad
    FROM 
      ciclista c 
    WHERE c.nomequipo='Amore Vita' 

  UNION
  
  SELECT DISTINCT
      c.edad 
    FROM 
      ciclista c 
    WHERE c.nomequipo='Artiach';


-- 4	Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.

  -- mediante OR

  SELECT 
      c.dorsal      -- proyeccion
    FROM 
      ciclista c    -- tabla
    WHERE 
      c.edad<25     -- pregunta 1
      OR            -- devuelve verdadero en cuanto una pregunta es verdadera
      c.edad>30;    -- pregunta 2

  -- mediante in, NO PODEMOS REALIZARLA

  -- mediante la UNION

    SELECT 
        c.dorsal 
      FROM 
        ciclista c 
      WHERE 
        c.edad<25
    
    UNION 
    
    SELECT 
        c.dorsal 
      FROM 
        ciclista c 
      WHERE 
        c.edad>30;
  
  

-- 5	Listar los dorsales de los ciclistas cuya edad 
--    este entre 28 y 32 y además que solo sean de Banesto.

-- utilizando AND 

  SELECT 
      c.dorsal 
    FROM 
      ciclista c
    WHERE
      c.edad >=28 AND  edad<=32 
      AND c.nomequipo='Banesto';

-- utilizando between 
  
  SELECT 
      c.dorsal 
    FROM 
      ciclista c
    WHERE
      c.edad BETWEEN 28 AND 32 
      AND c.nomequipo='Banesto';
    



  -- UTILIZANDO INTERSECCION
  
  /** mysql no implementa la interseccion
  SELECT 
      c.dorsal 
    FROM 
      ciclista c
    WHERE
      c.edad BETWEEN 28 AND 32 
  
  INTERSECT      
  
  SELECT 
      c.dorsal 
    FROM 
      ciclista c
    WHERE
      c.nomequipo='Banesto';
    */

  
  -- implementandolo con un natural join

 SELECT 
      * 
    FROM 
      (SELECT 
        c.dorsal 
      FROM 
        ciclista c
      WHERE
        c.edad BETWEEN 28 AND 32) AS c1 
  
    NATURAL JOIN 

      (SELECT 
        c.dorsal 
      FROM 
        ciclista c
      WHERE
        c.nomequipo='Banesto') AS c2;

  -- IMPLEMENTADO CON UN JOIN

SELECT 
      * 
    FROM 
      (SELECT 
        c.dorsal 
      FROM 
        ciclista c
      WHERE
        c.edad BETWEEN 28 AND 32) AS c1 
  
    JOIN 

      (SELECT 
        c.dorsal 
      FROM 
        ciclista c
      WHERE
        c.nomequipo='Banesto') AS c2
    
    ON c1.dorsal=c2.dorsal;

-- 6 Indícame el nombre de los ciclistas que 
-- el número de caracteres del nombre sea mayor que 8

SELECT DISTINCT 
    c.nombre
  FROM 
    ciclista c
  WHERE 
    CHAR_LENGTH(c.nombre)>8; -- la funcion char_length me cuenta los caracteres

  
-- 7 Lístame el nombre y el dorsal de todos los ciclistas 
-- mostrando un campo nuevo denominado nombre mayúsculas 
-- que debe mostrar el nombre en mayúsculas

SELECT 
    c.nombre,
    c.dorsal,
    UPPER(c.nombre) AS `nombre mayusculas`
  FROM 
    ciclista c;


-- 8 Listar todos los ciclistas que han llevado 
-- el maillot MGE (amarillo) en alguna etapa.

SELECT DISTINCT
    l.dorsal 
  FROM 
    lleva l
  WHERE
    l.código='MGE';
    

-- SI ME PIDEN EL RESTO DE CAMPOS DE CICLISTAS

  SELECT DISTINCT  
      c.* 
    FROM  
      lleva l JOIN ciclista c ON l.dorsal = c.dorsal
    WHERE
      l.código='MGE';


/*
  Consulta 9:
  Listar el nombre de los puertos cuya altura sea mayor que 1500
*/

  SELECT 
      p.nompuerto
    FROM 
      puerto p 
    WHERE 
      p.altura>1500;

  /*
  Consulta 10:
  Listar el dorsal de los ciclistas que hayan ganado 
  algun puerto cuya pendiente sea mayor que 8 
  o cuya altura este entre 1800 y 3000
*/
  
SELECT DISTINCT
    p.dorsal 
  FROM 
    puerto p
  WHERE 
      p.pendiente>8
    OR
      p.altura BETWEEN 1800 AND 3000;
    

  -- REALIZAR A TRAVES DE UNA UNION

  -- C2 : CICLISTAS QUE HAN GANADO PUERTOS ENTRE 1800 Y 3000
  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000;
  
  -- C1
  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.pendiente>8; 
  
  -- C1 UNION C2
  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000
  
  UNION 

   SELECT DISTINCT
      p.dorsal
   FROM 
      puerto p 
   WHERE 
      p.pendiente>8; 


/*
  Consulta 11:
  Listar el dorsal de los ciclistas que hayan ganado 
  algun puerto cuya pendiente sea mayor que 8 
  y cuya altura este entre 1800 y 3000
*/

  SELECT DISTINCT
    p.dorsal
  FROM 
    puerto p 
  WHERE 
    p.altura BETWEEN 1800 AND 3000 
    AND  
    p.pendiente>8;

-- esto es una interseccion y no es lo mismo que lo anterior


SELECT c1.dorsal
  FROM 
    (SELECT DISTINCT p.dorsal FROM puerto p WHERE p.altura BETWEEN 1800 AND 3000) AS c1
  NATURAL JOIN 
    (SELECT DISTINCT p.dorsal FROM puerto p WHERE p.pendiente>8) AS c2;
  

-- ESTO ES UNA INTERSECCION Y SI ES LO MISMO QUE LO ANTERIOR

SELECT * FROM 

(SELECT DISTINCT
  p.dorsal,p.nompuerto 
FROM puerto p
WHERE 
  p.altura BETWEEN 1800 AND 3000) c1
  NATURAL JOIN 

(SELECT DISTINCT
  p.dorsal,p.nompuerto 
FROM puerto p
WHERE 
  p.pendiente>8) c2 ;
